# Hapi Geocoding
Given a city location, we return the longitude and latitude of the city using Hapi.js framework and Redis Cache.
### Setup and config
1. Start Redis service
2. do `npm install` for the first time
3. Modify `config.ini` and provide with proper port/hostname details
4. start the program using :> `node gcService.js`
5. Access it using the following endpoint :> `http://[host]:[port]/{cityName}`. 
    - Ex. http://localhost:65501/chennai
6. Optional: API keys can be generated from google cloud platform

### Input and Output
Input is a city name as a `string`.

Output will be a `JSON` object in the following format
```
{
    lng: [longitute value],
    lat: [lattitude value]
}
```
Incase of error:
```
{
   error: [error message] 
}
```

Review the Geocoding API guide for possible Failure responses:> https://developers.google.com/maps/documentation/geocoding/intro#GeocodingResponses


