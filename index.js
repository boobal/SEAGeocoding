'use strict';

//Imports
const Wreck = require('wreck');
const Fs = require('fs');
const Ini = require('ini');

//config parsing
let config = Ini.parse(Fs.readFileSync('./config.ini', 'utf-8'));
//Google APIs
const baseURL = config.Google.baseURL;
const outputFormat = config.Google.outputFormat;
const apiKey = config.Google.apiKey;

//<--Helper functions-->

//Construct URL from the given inputs
const constructURL = async (cityName) => {
  const url = baseURL + outputFormat
              + '?components=locality:' + cityName + '&key=' + apiKey;
  return url;
};

//Get the lng, lat values as JSON object
const getCoordinates = async (payload) => {
  try {
    if(payload == null) {
      throw Error('Empty payload');
    }

    let response = JSON.parse(payload.toString());
    let status = response.status;
    console.log(response.status);
    let result = response.results[0];
    console.log(result);

    if(status !== 'OK') {
      throw Error('Unsuccessful API response:' + response.status);
    }

    return result.geometry.location;
  }
  catch(err) {
    throw err;
  }
};

//<--Exported function from geocoding module.-->
//Given a city name, Returns the lng, lat values as JSON object
async function geocode(cityName) {
  try {
    let url = await constructURL(cityName);
    let { res, payload } = await Wreck.get(url);
    let answer = await getCoordinates(payload);
    return answer;
  }
  catch (err) {
    console.log('Error for city:' + cityName);
    throw err;
  }
};

module.exports = geocode;
